# E05 - Anniversaires

!!! tip "Le problème"
    [Anniversaires](https://prologin.org/train/2011/semifinal/anniversaires)

## Solution

```python
"""
author: Franck CHAMBON
problem: https://prologin.org/train/2011/semifinal/anniversaires
"""

# 0. Cœur du problème
def nb_ballons(besoins: list) -> int:
    """Renvoie le nombre de ballons nécessaires suivant l'énoncé.

    >>> nb_ballons([20, 20])
    30

    >>> nb_ballons([10, 20, 30, 40])
    70

    >>> nb_ballons([80, 10, 80])
    125

    """
    stock = 0
    besoin_total = 0
    for besoin in besoins:
        if besoin > stock:
            besoin_total += besoin - stock
            stock = besoin // 2
        else:
            stock -= besoin // 2
    return besoin_total

import doctest
doctest.testmod()


# 1. Lecture
nb_anniversaires = int(input())
anniversaires = list(map(int, input().split()))

# 2. Écriture
print(nb_ballons(anniversaires))
```

