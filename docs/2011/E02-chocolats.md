# E02 - Chocolats

!!! tip "Le problème"
    [Chocolats](https://prologin.org/train/2011/semifinal/chocolats)

## Solution

```python
"""
author: Franck CHAMBON
problem: https://prologin.org/train/2011/semifinal/chocolats
"""
# 0. Cœur du problème
def moyenne_nombres_pairs(ma_liste: list[int]) -> int:
    """Renvoie la moyenne tronquée des nombres pairs de `ma_liste`
    >>> moyenne_nombres_pairs([1, 2, 3, 4])
    3
    >>> moyenne_nombres_pairs([12, 22, 3, 0])
    11
    """
    ma_liste_filtree = [x for x in ma_liste if x % 2 == 0]
    moyenne_filtree_tronquee = sum(ma_liste_filtree) // len(ma_liste_filtree)
    return moyenne_filtree_tronquee

import doctest
doctest.testmod()

# 1. Lecture
nb_chocolats = int(input())
chocolats = list(map(int, input().split()))

# 2. Écriture
print(moyenne_nombres_pairs(chocolats))
```

### Variante

Ceci était une version fonctionnelle, on peut écrire un code plus simple.

```python
nb_tours = int(input())
nb_chocolats = 0
nb_couples = 0
for x in map(int, input().split()):
    if x % 2 == 0:
        nb_couples += 1
        nb_chocolats += x
print(nb_chocolats // nb_couples)
```
