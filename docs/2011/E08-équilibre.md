# E08 - Équilibre

!!! tip "Le problème"
    [Équilibre](https://prologin.org/train/2011/semifinal/equilibre)

## Solution

```python
"""
author: Franck CHAMBON
problem: https://prologin.org/train/2011/semifinal/equilibre
"""

# 0. Initialisation
somme_ponderee_x = 0.0
somme_ponderee_y = 0.0
somme_poids = 0.0

# 1. Lecture et cumul
nb_objets = int(input())
for i in range(nb_objets):
    x, y, poids = map(float, input().split())
    somme_ponderee_x += x * poids
    somme_ponderee_y += y * poids
    somme_poids      += poids

# 2. Calcul et écriture
centre_gravite_x = somme_ponderee_x / somme_poids
centre_gravite_y = somme_ponderee_y / somme_poids
print(int(centre_gravite_x), int(centre_gravite_y))
```
