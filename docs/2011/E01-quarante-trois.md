# E01 - Quarante-trois

!!! tip "Le problème"
    [Quarante-trois](https://prologin.org/train/2011/semifinal/43)

## Solution

```python
"""
author: Franck CHAMBON
problem: https://prologin.org/train/2011/semifinal/43
"""

un_entier = int(input())

print(un_entier - 1)
```

## Commentaire

Juste pour se chauffer !
