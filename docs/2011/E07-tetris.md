# E07 - Tetris

!!! tip "Le problème"
    [Tetris](https://prologin.org/train/2011/semifinal/tetris)

## Solution

```python
"""
author: Franck CHAMBON
problem: https://prologin.org/train/2011/semifinal/tetris
"""

# 0. Cœur du problème
def nb_suppressions(tetris: list[int]) -> int:
    """Renvoie le nombre de lignes que l'on peut supprimer dans
    un Tetris, un tableau 2D rempli de 0 et 1.

    >>> nb_suppressions([[0, 0, 0, 0, 0], [1, 1, 1, 1, 1], [1, 1, 0, 1, 1]])
    1

    Astuce : si la somme d'une ligne est égale à nb_colonnes,
             alors on peut la supprimer.
    """
    if len(tetris) == 0:
        return 0 # Tetris vide
    nb_colonnes = len(tetris[0])

    # version fonctionnelle
    return sum(1 for ligne in tetris if sum(ligne) == nb_colonnes)
    
    # version itérative
    nb_lignes_pleines = 0
    for ligne in tetris:
        if sum(ligne) == nb_colonnes:
            nb_lignes_pleines += 1
    return nb_lignes_pleines

# tests
import doctest
doctest.testmod()

# 1. Lecture
nb_lignes = int(input())
nb_colonnes = int(input())
tetris = [list(map(int, input().split())) for _ in range(nb_lignes)]

# 2. Écriture
print(nb_suppressions(tetris))
```
