# Prologin

On propose ici des solutions pour Prologin en Python pour les élèves de NSI (surtout en terminale).

!!! tip "Pour progresser"
    Ces solutions alternatives du professeur, avec commentaires, sont à étudier après avoir déjà résolu le problème.
